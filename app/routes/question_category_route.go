package routes

import (
	"receipt/application/controllers"

	"github.com/labstack/echo"
)

func QuestionCategory(g *echo.Group) {

	DEFINE_URL := "/master_data/question_category"

	g.GET(DEFINE_URL+"/", controllers.ListQuestionCategoryController)
	g.GET(DEFINE_URL+"/addform/", controllers.AddQuestionCategoryController)
	g.POST(DEFINE_URL+"/addform/", controllers.StoreQuestionCategoryController)
	g.GET(DEFINE_URL+"/editform/:id/", controllers.EditQuestionCategoryController)
	g.POST(DEFINE_URL+"/editform/:id/", controllers.UpdateQuestionCategoryController)
	g.POST(DEFINE_URL+"/deleteform/:id/", controllers.DeleteQuestionCategoryController)
	g.POST(DEFINE_URL+"/delete_all/:id/", controllers.DeleteAllQuestionCategoryController)
}

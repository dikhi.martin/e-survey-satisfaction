package routes

import (
	"receipt/application/controllers"

	"github.com/labstack/echo"
)

func Question(g *echo.Group) {

	DEFINE_URL := "/master_data/question"

	g.GET(DEFINE_URL+"/", controllers.ListQuestionController)
	g.GET(DEFINE_URL+"/addform/", controllers.AddQuestionController)
	g.POST(DEFINE_URL+"/addform/", controllers.StoreQuestionController)
	g.GET(DEFINE_URL+"/editform/:id/", controllers.EditQuestionController)
	g.POST(DEFINE_URL+"/editform/:id/", controllers.UpdateQuestionController)
	g.POST(DEFINE_URL+"/deleteform/:id/", controllers.DeleteQuestionController)
	g.POST(DEFINE_URL+"/delete_all/:id/", controllers.DeleteAllQuestionController)
}

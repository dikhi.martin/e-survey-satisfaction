package controllers

import (
	"receipt/application/models"
	"receipt/database"
	"strings"

	"github.com/dikhimartin/beego-v1.12.0/utils/pagination"
	"github.com/labstack/echo"
)

// == Custom Function
func GetDataQuestionById(id_question string /*convert_to_md5*/) models.ModelQuestion {
	db := database.CreateCon()
	defer db.Close()

	var id, id_setting_category, question, status []byte
	row := db.Table("tb_setting_question").Where("md5(id) = ?", id_question).Select("id, id_setting_category, question, status").Row()
	err := row.Scan(&id, &id_setting_category, &question, &status)
	if err != nil {
		logs.Println(err)
	}
	data := models.ModelQuestion{
		ID:                  string(id),
		Id_setting_category: string(id_setting_category),
		Question:            string(question),
		Status:              string(status),
		Additional:          id_question,
	}
	return data
}

func GetDataQuestion() []models.ModelQuestion {
	db := database.CreateCon()
	defer db.Close()

	rows, err := db.Raw("SELECT id, id_setting_category, question, status FROM tb_setting_question WHERE status = 'active' ORDER BY name").Rows()
	if err != nil {
		logs.Println(err)
	}
	defer rows.Close()
	each := models.ModelQuestion{}
	result := []models.ModelQuestion{}

	for rows.Next() {
		var id, id_setting_category, question, status []byte
		err = rows.Scan(&id, &id_setting_category, &question, &status)
		if err != nil {
			logs.Println(err)
		}

		each.ID = string(id)
		each.Id_setting_category = string(id_setting_category)
		each.Question = string(question)
		each.Status = string(status)

		result = append(result, each)
	}

	return result
}

// == View
func ListQuestionController(c echo.Context) error {
	db := database.CreateCon()
	defer db.Close()

	// data_users := GetDataLogin(c)
	// if CheckPrivileges(data_users.Id_group, "samplecrud_2") == false {
	// 	return c.Render(403, "error_403", nil)
	// }

	// delete_permission := CheckPrivileges(data_users.Id_group, "samplecrud_4")

	var whrs string
	var search string
	var searchStatus string

	if reqSearch := c.FormValue("search"); reqSearch != "" {
		search = reqSearch
	}
	if reqSearchStatus := c.FormValue("searchStatus"); reqSearchStatus != "" {
		searchStatus = reqSearchStatus
	}

	selected := "SELECT question.id, category.id As id_setting_category,category.name AS name_category, question.question, question.created_at, question.updated_at, question.status"
	join := " LEFT JOIN tb_setting_question_category AS category on question.id_setting_category = category.id"
	if search != "" {
		ors := " FROM tb_setting_question AS question WHERE concat(id_setting_category, question) LIKE '%" + search + "%' "
		whrs += ors + join
	} else if searchStatus != "" && searchStatus == "Y" {
		ors := " FROM tb_setting_question AS question WHERE question.status = '" + searchStatus + "' "
		whrs += ors + join
	} else if searchStatus != "" && searchStatus == "N" {
		ors := " FROM tb_setting_question AS question WHERE question.status = '" + searchStatus + "' "
		whrs += ors + join
	} else {
		whrs = " FROM tb_setting_question AS question" + join
	}

	rows, err := db.Raw(selected + whrs + " ORDER BY id DESC").Rows()
	if err != nil {
		logs.Println(err)
		return c.Render(500, "error_500", nil)
	}

	each := models.ModelQuestion{}
	result := []models.ModelQuestion{}

	for rows.Next() {
		var id, id_setting_category, name_category, question, created_at, updated_at, status []byte
		var err = rows.Scan(&id, &id_setting_category, &name_category, &question, &created_at, &updated_at, &status)
		if err != nil {
			logs.Println(err)
			return c.Render(500, "error_500", nil)
		}

		each.ID = ConvertToMD5(string(id))
		each.Id_setting_category = string(id_setting_category)
		each.Name_category = string(name_category)
		each.Question = string(question)
		each.Status = string(status)
		each.CreatedAt = FormatDate(string(created_at), "02 January 2006 at 15:04 PM")
		each.UpdatedAt = FormatDate(string(updated_at), "02 January 2006 at 15:04 PM")

		result = append(result, each)
	}

	postsPerPage := 10
	paginator = pagination.NewPaginator(c.Request(), postsPerPage, len(result))

	// fetch the next posts "postsPerPage"
	idrange := NewSlice(paginator.Offset(), postsPerPage, 1)
	mydatas := []models.ModelQuestion{}
	for _, num := range idrange {
		if num <= len(result)-1 {
			numdata := result[num]
			mydatas = append(mydatas, numdata)
		}
	}

	data := response_json{
		"delete_permission": "delete_permission",
		"paginator":         paginator,
		"data":              mydatas,
		"search":            search,
		"searchStatus":      searchStatus,
	}

	return c.Render(200, "list_question", data)
}

func AddQuestionController(c echo.Context) error {
	db := database.CreateCon()
	defer db.Close()
	// data_users := GetDataLogin(c)
	// if CheckPrivileges(data_users.Id_group, "samplecrud_1") == false {
	// 	return c.Render(403, "error_403", nil)
	// }

	category := GetDataQuestionCategory()
	data := response_json{
		"category": category,
	}

	return c.Render(200, "add_form_question", data)
}

func EditQuestionController(c echo.Context) error {
	db := database.CreateCon()
	defer db.Close()

	// data_users := GetDataLogin(c)
	// if CheckPrivileges(data_users.Id_group, "samplecrud_3") == false {
	// 	return c.Render(403, "error_403", nil)
	// }

	requested_id := c.Param("id")
	data := GetDataQuestionById(requested_id)
	category := GetDataQuestionCategory()

	response := response_json{
		"data":     data,
		"category": category,
	}

	return c.Render(200, "edit_form_question", response)
}

// == Manipulate
func StoreQuestionController(c echo.Context) error {
	db := database.CreateCon()
	defer db.Close()

	// data_users := GetDataLogin(c)
	// if CheckPrivileges(data_users.Id_group, "samplecrud_1") == false {
	// 	return c.Render(403, "error_403", nil)
	// }

	id_setting_category := c.FormValue("id_setting_category")
	question := c.FormValue("question")
	status := c.FormValue("status")

	// insert_data
	insert := models.Question{
		Id_setting_category: ConvertStringToInt(id_setting_category),
		Question:            question,
		Status:              status,
		// Created_by:  data_users.Id_user,
		CreatedAt: current_time("2006-01-02 15:04:05"),
	}
	if error_insert := db.Create(&insert); error_insert.Error != nil {
		logs.Println(error_insert)
		return c.Render(500, "error_500", nil)

	}
	db.NewRecord(insert)
	return c.Redirect(301, "/lib/master_data/question/")
}

func UpdateQuestionController(c echo.Context) error {
	db := database.CreateCon()
	defer db.Close()

	// data_users := GetDataLogin(c)
	// if CheckPrivileges(data_users.Id_group, "samplecrud_3") == false {
	// 	return c.Render(403, "error_403", nil)
	// }

	requested_id := c.Param("id")

	id_setting_category := c.FormValue("id_setting_category")
	question := c.FormValue("question")
	status := c.FormValue("status")

	// update_data
	var update models.Question
	update_data := db.Model(&update).Where("md5(id) = ?", requested_id).Updates(map[string]interface{}{
		"id_setting_category": ConvertStringToInt(id_setting_category),
		"question":            question,
		// "updated_by": data_users.Id_user,
		"status":     status,
		"updated_at": current_time("2006-01-02 15:04:05"),
	})
	if update_data.Error != nil {
		logs.Println(update_data.Error)
		return c.Render(500, "error_500", nil)
	}

	return c.Redirect(301, "/lib/master_data/question/")
}

func DeleteQuestionController(c echo.Context) error {
	db := database.CreateCon()
	defer db.Close()

	// data_users := GetDataLogin(c)
	// if CheckPrivileges(data_users.Id_group, "samplecrud_4") == false {
	// 	return c.JSON(200, false)
	// }

	requested_id := c.Param("id")
	var model models.Question
	delete := db.Unscoped().Where("md5(id) = ?", requested_id).Delete(&model)
	if delete.Error != nil {
		logs.Println(delete.Error)
		return c.JSON(200, false)
	}

	return c.JSON(200, true)
}

func DeleteAllQuestionController(c echo.Context) error {
	db := database.CreateCon()
	defer db.Close()

	// data_users := GetDataLogin(c)
	// if CheckPrivileges(data_users.Id_group, "samplecrud_4") == false {
	// 	return c.JSON(200, false)
	// }

	requested_id := c.Param("id")

	result := strings.Split(requested_id, ",")

	for i := range result {
		var model models.Question
		delete := db.Unscoped().Where("md5(id) = ?", result[i]).Delete(&model)
		if delete.Error != nil {
			logs.Println(delete.Error)
			return c.JSON(200, false)
		}
	}

	return c.JSON(200, true)
}

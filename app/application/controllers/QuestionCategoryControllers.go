package controllers

import (
	"receipt/application/models"
	"receipt/database"
	"strings"

	"github.com/dikhimartin/beego-v1.12.0/utils/pagination"
	"github.com/labstack/echo"
)

// == Custom Function
func GetDataQuestionCategoryById(id_question_category string /*convert_to_md5*/) models.ModelQuestionCategory {
	db := database.CreateCon()
	defer db.Close()

	var id, name, description, status []byte
	row := db.Table("tb_setting_question_category").Where("md5(id) = ?", id_question_category).Select("id, name, description, status").Row()
	err := row.Scan(&id, &name, &description, &status)
	if err != nil {
		logs.Println(err)
	}
	data := models.ModelQuestionCategory{
		ID:          string(id),
		Name:        string(name),
		Description: string(description),
		Status:      string(status),
		Additional:  id_question_category,
	}
	return data
}

func GetDataQuestionCategory() []models.ModelQuestionCategory {
	db := database.CreateCon()
	defer db.Close()

	rows, err := db.Raw("SELECT id, name, description, status FROM tb_setting_question_category WHERE status = 'active' ORDER BY name").Rows()
	if err != nil {
		logs.Println(err)
	}
	defer rows.Close()
	each := models.ModelQuestionCategory{}
	result := []models.ModelQuestionCategory{}

	for rows.Next() {
		var id, name, description, status []byte
		err = rows.Scan(&id, &name, &description, &status)
		if err != nil {
			logs.Println(err)
		}

		each.ID = string(id)
		each.Name = string(name)
		each.Description = string(description)
		each.Status = string(status)

		result = append(result, each)
	}

	return result
}

// == View
func ListQuestionCategoryController(c echo.Context) error {
	db := database.CreateCon()
	defer db.Close()

	// data_users := GetDataLogin(c)
	// if CheckPrivileges(data_users.Id_group, "samplecrud_2") == false {
	// 	return c.Render(403, "error_403", nil)
	// }

	// delete_permission := CheckPrivileges(data_users.Id_group, "samplecrud_4")

	var selected string
	var whrs string
	var search string
	var searchStatus string

	if reqSearch := c.FormValue("search"); reqSearch != "" {
		search = reqSearch
	}
	if reqSearchStatus := c.FormValue("searchStatus"); reqSearchStatus != "" {
		searchStatus = reqSearchStatus
	}

	selected = "SELECT id, name, description, created_by, created_at, updated_at, status"
	if search != "" {
		ors := " FROM tb_setting_question_category WHERE concat(name, description) LIKE '%" + search + "%' "
		whrs += ors
	} else if searchStatus != "" && searchStatus == "Y" {
		ors := " FROM tb_setting_question_category WHERE status = '" + searchStatus + "' "
		whrs += ors
	} else if searchStatus != "" && searchStatus == "N" {
		ors := " FROM tb_setting_question_category WHERE status = '" + searchStatus + "' "
		whrs += ors
	} else {
		whrs = " FROM tb_setting_question_category"
	}

	rows, err := db.Raw(selected + whrs + " ORDER BY id DESC").Rows()
	if err != nil {
		logs.Println(err)
		return c.Render(500, "error_500", nil)
	}

	each := models.ModelQuestionCategory{}
	result := []models.ModelQuestionCategory{}

	for rows.Next() {
		var id, name, description, created_by, created_at, updated_at, status []byte
		var err = rows.Scan(&id, &name, &description, &created_by, &created_at, &updated_at, &status)
		if err != nil {
			logs.Println(err)
			return c.Render(500, "error_500", nil)
		}

		each.ID = ConvertToMD5(string(id))
		each.Name = string(name)
		each.Description = string(description)
		each.Status = string(status)
		each.CreatedAt = FormatDate(string(created_at), "02 January 2006 at 15:04 PM")
		each.UpdatedAt = FormatDate(string(updated_at), "02 January 2006 at 15:04 PM")

		result = append(result, each)
	}

	postsPerPage := 10
	paginator = pagination.NewPaginator(c.Request(), postsPerPage, len(result))

	// fetch the next posts "postsPerPage"
	idrange := NewSlice(paginator.Offset(), postsPerPage, 1)
	mydatas := []models.ModelQuestionCategory{}
	for _, num := range idrange {
		if num <= len(result)-1 {
			numdata := result[num]
			mydatas = append(mydatas, numdata)
		}
	}

	data := response_json{
		"delete_permission": "delete_permission",
		"paginator":         paginator,
		"data":              mydatas,
		"search":            search,
		"searchStatus":      searchStatus,
	}

	return c.Render(200, "list_question_category", data)
}

func AddQuestionCategoryController(c echo.Context) error {
	db := database.CreateCon()
	defer db.Close()
	// data_users := GetDataLogin(c)
	// if CheckPrivileges(data_users.Id_group, "samplecrud_1") == false {
	// 	return c.Render(403, "error_403", nil)
	// }

	return c.Render(200, "add_form_question_category", "data")
}

func EditQuestionCategoryController(c echo.Context) error {
	db := database.CreateCon()
	defer db.Close()

	// data_users := GetDataLogin(c)
	// if CheckPrivileges(data_users.Id_group, "samplecrud_3") == false {
	// 	return c.Render(403, "error_403", nil)
	// }

	requested_id := c.Param("id")
	data := GetDataQuestionCategoryById(requested_id)

	response := response_json{
		"data": data,
	}

	return c.Render(200, "edit_form_question_category", response)
}

// == Manipulate
func StoreQuestionCategoryController(c echo.Context) error {
	db := database.CreateCon()
	defer db.Close()

	// data_users := GetDataLogin(c)
	// if CheckPrivileges(data_users.Id_group, "samplecrud_1") == false {
	// 	return c.Render(403, "error_403", nil)
	// }

	name := c.FormValue("name")
	description := c.FormValue("description")
	status := c.FormValue("status")

	// insert_data
	insert := models.QuestionCategory{
		Name:        name,
		Description: description,
		Status:      status,
		// Created_by:  data_users.Id_user,
		CreatedAt: current_time("2006-01-02 15:04:05"),
	}
	if error_insert := db.Create(&insert); error_insert.Error != nil {
		logs.Println(error_insert)
		return c.Render(500, "error_500", nil)

	}
	db.NewRecord(insert)
	return c.Redirect(301, "/lib/master_data/question_category/")
}

func UpdateQuestionCategoryController(c echo.Context) error {
	db := database.CreateCon()
	defer db.Close()

	// data_users := GetDataLogin(c)
	// if CheckPrivileges(data_users.Id_group, "samplecrud_3") == false {
	// 	return c.Render(403, "error_403", nil)
	// }

	requested_id := c.Param("id")

	name := c.FormValue("name")
	description := c.FormValue("description")
	status := c.FormValue("status")

	// update_data
	var update models.QuestionCategory
	update_data := db.Model(&update).Where("md5(id) = ?", requested_id).Updates(map[string]interface{}{
		"name":        name,
		"description": description,
		// "updated_by": data_users.Id_user,
		"status":     status,
		"updated_at": current_time("2006-01-02 15:04:05"),
	})
	if update_data.Error != nil {
		logs.Println(update_data.Error)
		return c.Render(500, "error_500", nil)
	}

	return c.Redirect(301, "/lib/master_data/question_category/")
}

func DeleteQuestionCategoryController(c echo.Context) error {
	db := database.CreateCon()
	defer db.Close()

	// data_users := GetDataLogin(c)
	// if CheckPrivileges(data_users.Id_group, "samplecrud_4") == false {
	// 	return c.JSON(200, false)
	// }

	requested_id := c.Param("id")
	var model models.QuestionCategory
	delete := db.Unscoped().Where("md5(id) = ?", requested_id).Delete(&model)
	if delete.Error != nil {
		logs.Println(delete.Error)
		return c.JSON(200, false)
	}

	return c.JSON(200, true)
}

func DeleteAllQuestionCategoryController(c echo.Context) error {
	db := database.CreateCon()
	defer db.Close()

	// data_users := GetDataLogin(c)
	// if CheckPrivileges(data_users.Id_group, "samplecrud_4") == false {
	// 	return c.JSON(200, false)
	// }

	requested_id := c.Param("id")

	result := strings.Split(requested_id, ",")

	for i := range result {
		var model models.QuestionCategory
		delete := db.Unscoped().Where("md5(id) = ?", result[i]).Delete(&model)
		if delete.Error != nil {
			logs.Println(delete.Error)
			return c.JSON(200, false)
		}
	}

	return c.JSON(200, true)
}

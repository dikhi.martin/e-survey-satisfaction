package models

// its use for definition database GORM
type QuestionCategory struct {
	ID          int    `gorm:"AUTO_INCREMENT;PRIMARY_KEY"`
	Name        string `gorm:"type:varchar(191)"`
	Description string `gorm:"type:varchar(255)"`
	Created_by  int    `gorm:"type:int(10); index;"`
	Updated_by  int    `gorm:"type:int(10); index;"`
	Status      string `gorm:"type:enum('active','inactive'); comment:'Y:Active, N:Inactive'; default:'active'"`
	CreatedAt   string `gorm:"type:timestamp(0); default:CURRENT_TIMESTAMP"`
	UpdatedAt   string `gorm:"type:timestamp(0); default:CURRENT_TIMESTAMP"`
	Additional  string `gorm:"type:varchar(191)"`
}

func (QuestionCategory) TableName() string {
	return "tb_setting_question_category"
}

// its use for call model from controllers
type ModelQuestionCategory struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Status      string `json:"status"`
	CreatedAt   string `json:"created_at"`
	UpdatedAt   string `json:"updated_at"`
	Additional  string `json:"additional"`
}

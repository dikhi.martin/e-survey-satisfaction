package models

// its use for definition database GORM
type Question struct {
	ID                  int    `gorm:"AUTO_INCREMENT;PRIMARY_KEY"`
	Id_setting_category int    `gorm:"type:int(10); index; NOT NULL"`
	Question            string `gorm:"type:text(0)"`
	Created_by          int    `gorm:"type:int(10); index;"`
	Updated_by          int    `gorm:"type:int(10); index;"`
	Status              string `gorm:"type:enum('active','inactive'); comment:'Y:Active, N:Inactive'; default:'active'"`
	CreatedAt   		string `gorm:"type:timestamp(0); default:CURRENT_TIMESTAMP"`
	UpdatedAt   		string `gorm:"type:timestamp(0); default:CURRENT_TIMESTAMP"`
	Additional          string `gorm:"type:varchar(191)"`
}

func (Question) TableName() string {
	return "tb_setting_question"
}

// its use for call model from controllers
type ModelQuestion struct {
	ID                  string `json:"id"`
	Id_setting_category string `json:"id_setting_category"`
	Name_category       string `json:"name_category"`
	Question            string `json:"question"`
	Status              string `json:"status"`
	CreatedAt           string `json:"created_at"`
	UpdatedAt           string `json:"updated_at"`
	Additional          string `json:"additional"`
}
